using System;
using Xunit;
using GradeBook;

namespace Gradebook.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {

            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(99.1);
            book.AddGrade(10.3);

            //arrange
            // var x=5;
            // var y=2;
            // var expected=7;

            //act
            // var actual=x*y;
            //assert
            Assert.Equal(10.3,book.GetLowestGrade());//low
            Assert.Equal(99.1,book.GetHighestGrade());//high
        }
    }
}
