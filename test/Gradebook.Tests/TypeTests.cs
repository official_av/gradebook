using System;
using Xunit;
using GradeBook;

namespace Gradebook.Tests{
    public class TypeTests{
        [Fact]
        public void TestName()
        {
            var book1=GetBook("yo");
            var book2=GetBook("yo");
            Assert.NotSame(book1,book2);
        }

        Book GetBook(string s){
            return new Book(s);
        }
    }
}