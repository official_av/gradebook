﻿using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book("Science");
            book.AddGrade(89.1);
            book.AddGrade(99.1);
            book.AddGrade(10.3);
            book.ShowStatistics();
        }
    }
}
