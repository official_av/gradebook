using System.Collections.Generic;
using System;

namespace GradeBook {
    public class Book {
        private List<double> grades;
        private string name;

        public Book(string name) {
            this.name=name;
            grades=new List<double>();
        }

        public void AddGrade(double grade){
            grades.Add(grade);
        }

        public List<double> GetGrades(){
            return this.grades;
        }

        public double GetLowestGrade(){
            double min=double.MaxValue;
            this.grades.ForEach(x=>{
                min=Math.Min(x,min);
            });
            return min;
        }

        public double GetHighestGrade(){
            double max=double.MinValue;
            this.grades.ForEach(x=>{
                max=Math.Max(x,max);
            });
            return max;
        }

        public double GetAverageGrade(){
            double sum=0.0;
            this.grades.ForEach(x=>{
                sum+=x;
            });
            return sum/this.grades.Count;
        }

        public void ShowStatistics(){
            System.Console.WriteLine($"Lowest grade is {this.GetLowestGrade():N1}.");
            System.Console.WriteLine($"Average grade is {this.GetAverageGrade():N1}.");
            System.Console.WriteLine($"Highest grade is {this.GetHighestGrade():N1}.");
        }
    }
}